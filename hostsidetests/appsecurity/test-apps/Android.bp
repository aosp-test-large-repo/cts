//
// Copyright (C) 2017 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

android_test_helper_app {
    name: "CtsUsePermissionApp25",
    defaults: ["cts_support_defaults"],
    static_libs: [
        "compatibility-device-util-axt",
        "ctstestrunner-axt",
    ],
    libs: [
        "CtsExternalStorageTestLib",
    ],
    manifest: "UsePermissionApp25/AndroidManifest.xml",
    srcs: [
        "UsePermissionApp23/src/com/android/cts/usepermission/UsePermissionTest23.java",
        ":CtsAppSecurityBasePermissionSrc",
    ],
    resource_dirs: ["UsePermissionApp23/res"],
    // For ACCESS_BACKGROUND_LOCATION
    platform_apis: true,
    // tag this module as a cts test artifact
    test_suites: [
        "cts",
        "vts10",
        "general-tests",
        "mts",
    ],
    certificate: ":cts-testkey2",
    optimize: {
        enabled: false,
    },
    dex_preopt: {
        enabled: false,
    },
    min_sdk_version: "25",
}

android_test_helper_app {
    name: "CtsUsePermissionApp26",
    defaults: ["cts_support_defaults"],
    static_libs: [
        "compatibility-device-util-axt",
        "ctstestrunner-axt",
    ],
    srcs: [
        "UsePermissionApp26/src/**/*.java",
        ":CtsAppSecurityBasePermissionSrc",
    ],
    manifest: "UsePermissionApp26/AndroidManifest.xml",
    resource_dirs: ["UsePermissionApp23/res"],
    sdk_version: "test_current",
    min_sdk_version: "26",
    // tag this module as a cts test artifact
    test_suites: [
        "cts",
        "vts10",
        "general-tests",
        "mts",
    ],
    certificate: ":cts-testkey2",
    optimize: {
        enabled: false,
    },
    dex_preopt: {
        enabled: false,
    },
}

android_test_helper_app {
    name: "CtsUsePermissionApp28",
    defaults: ["cts_support_defaults"],
    static_libs: [
        "compatibility-device-util-axt",
        "ctstestrunner-axt",
    ],
    srcs: [
        "UsePermissionApp28/src/**/*.java",
        ":CtsAppSecurityBasePermissionSrc",
    ],
    manifest: "UsePermissionApp28/AndroidManifest.xml",
    resource_dirs: ["UsePermissionApp23/res"],
    // For ACCESS_BACKGROUND_LOCATION
    platform_apis: true,
    // tag this module as a cts test artifact
    test_suites: [
        "cts",
        "vts10",
        "general-tests",
        "mts",
    ],
    certificate: ":cts-testkey2",
    optimize: {
        enabled: false,
    },
    dex_preopt: {
        enabled: false,
    },
    min_sdk_version: "28",
}

android_test_helper_app {
    name: "CtsUsePermissionApp29",
    defaults: ["cts_support_defaults"],
    static_libs: [
        "compatibility-device-util-axt",
        "ctstestrunner-axt",
    ],

    srcs: [
        "UsePermissionApp29/src/**/*.java",
        ":CtsAppSecurityBasePermissionSrc",
    ],
    manifest: "UsePermissionApp29/AndroidManifest.xml",
    resource_dirs: ["UsePermissionApp23/res"],
    // For ACCESS_BACKGROUND_LOCATION
    platform_apis: true,
    // tag this module as a cts test artifact
    test_suites: [
        "cts",
        "vts10",
        "general-tests",
        "mts",
    ],
    certificate: ":cts-testkey2",
}

android_test_helper_app {
    name: "RequestsOnlyCalendarApp22",
    defaults: ["cts_support_defaults"],
    static_libs: [
        "compatibility-device-util-axt",
        "ctstestrunner-axt",
    ],
    libs: ["CtsExternalStorageTestLib"],
    srcs: [
        "UsePermissionApp22/src/**/*.java",
        ":CtsAppSecurityBasePermissionSrc",
    ],
    manifest: "RequestsOnlyCalendarApp22/AndroidManifest.xml",
    resource_dirs: ["UsePermissionApp22/res"],
    // For ACCESS_BACKGROUND_LOCATION
    platform_apis: true,
    // tag this module as a cts test artifact
    test_suites: [
        "cts",
        "vts10",
        "general-tests",
        "mts",
    ],
    certificate: ":cts-testkey2",
    min_sdk_version: "22",
}

android_test_helper_app {
    name: "CtsUsePermissionAppLatest",
    defaults: ["cts_support_defaults"],

    static_libs: [
        "androidx.test.rules",
        "compatibility-device-util-axt",
        "ctstestrunner-axt",
        "ub-uiautomator",
    ],

    srcs: [
        "UsePermissionApp26/src/**/*.java",
        "UsePermissionApp29/src/**/*.java",
        "UsePermissionApp23/src/com/android/cts/usepermission/BasePermissionActivity.java",
        "UsePermissionApp23/src/com/android/cts/usepermission/BasePermissionsTest.java",
    ],
    manifest: "UsePermissionAppLatest/AndroidManifest.xml",
    resource_dirs: ["UsePermissionApp23/res"],
    // For ACCESS_BACKGROUND_LOCATION
    platform_apis: true,
    // tag this module as a cts test artifact
    test_suites: [
        "cts",
        "vts10",
        "general-tests",
        "mts",
    ],
    certificate: ":cts-testkey2",
}
